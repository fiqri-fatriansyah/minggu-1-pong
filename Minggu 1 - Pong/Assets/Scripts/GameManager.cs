﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Pemain 1
    public PlayerControl player1; //Skrip
    private Rigidbody2D p1RigidBody;

    //Pemain 2
    public PlayerControl player2; //Skrip
    private Rigidbody2D p2RigidBody;

    //Bola
    public BallControl ball; //Skrip
    private Rigidbody2D ballRigidBody;
    private CircleCollider2D ballCollider;

    //Skor maksimal
    public int maxScore;

    //Status tampilan debug window.
    private bool isDebugWindowShown = false;

    //Objek untuk menggambar prediksi lintasan bola.
    public Trajectory trajectory;

    //Objek background.
    public SpriteRenderer background;
    
    // Start is called before the first frame update
    void Start()
    {
        //Inisiasi Rigidbody dan Collider.
        p1RigidBody = player1.GetComponent<Rigidbody2D>();
        p2RigidBody = player2.GetComponent<Rigidbody2D>();
        ballRigidBody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    //Untuk menampilkan GUI.
    void OnGUI()
    {
        //Tampilkan skor pemain 1 di kiri atas dan pemain 2 di kanan atas.
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), "" + player1.Score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + player2.Score);

        //Tombol restart untuk memulai kembali permainan dari awal.
        if (GUI.Button(new Rect(Screen.width / 2 - 60, 10, 120, 53), "ULANG"))
        {
            player1.ResetScore();
            player2.ResetScore();
            ball.gameObject.SendMessage("RestartGame", null, SendMessageOptions.RequireReceiver);
        }

        //Jika salah satu pemain mencapai skor maksimal, tampilkan layar kemenangan pada bagian kiri/kanan layar dan kembalikan bola.
        if (player1.Score == maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PEMAIN 1 MENANG!");
            ball.gameObject.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }
        else if (player2.Score == maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PEMAIN 2 MENANG!");
            ball.gameObject.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        // Jika isDebugWindowShown == true, tampilkan text area untuk debug window.
        if (isDebugWindowShown)
        {
            //Simpan nilai warna lama GUI.
            Color oldColor = GUI.backgroundColor;

            //Beri warna baru.
            GUI.backgroundColor = Color.red;

            //Simpan variabel-variabel fisika yang akan ditampilkan. 
            float ballMass = ballRigidBody.mass;
            Vector2 ballVelocity = ballRigidBody.velocity;
            float ballSpeed = ballRigidBody.velocity.magnitude;
            Vector2 ballMomentum = ballMass * ballVelocity;
            float ballFriction = ballCollider.friction;

            float impulsePlayer1X = player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;

            //Tentukan debug text-nya.
            string debugText =
                "Ball mass = " + ballMass + "\n" +
                "Ball velocity = " + ballVelocity + "\n" +
                "Ball speed = " + ballSpeed + "\n" +
                "Ball momentum = " + ballMomentum + "\n" +
                "Ball friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1X + ", " + impulsePlayer1Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2X + ", " + impulsePlayer2Y + ")\n";

            //Tampilkan debug window.
            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);

            //Kembalikan warna lama GUI
            GUI.backgroundColor = oldColor;
        }
        //Toggle nilai debug window ketika pemain mengeklik tombol ini.
        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO"))
        {
            isDebugWindowShown = !isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;
            background.enabled = !background.enabled;
        }
    }
}
