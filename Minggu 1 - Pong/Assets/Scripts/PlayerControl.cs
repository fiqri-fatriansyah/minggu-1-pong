﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    //Tombol untuk gerak raket ke atas.
    public KeyCode upButton = KeyCode.W;

    //Tombol untuk gerak raket ke bawah.
    public KeyCode downButton = KeyCode.S;

    //Kecepatan gerak raket.
    public float speed = 20f;

    //Batas atas dan batas bawah Scene. Tambahkan minus untuk posisi ke bawah.
    public float yBoundary = 6.5f;

    //Rigidbody raket.
    private Rigidbody2D rigidBody2D;

    //Skor pemain.
    private int score;

    //Titik tumbukan terakhir dengan bola untuk menampilkan variabel-variabel fisika terkait tumbukan tersebut.
    private ContactPoint2D lastContactPoint;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Ambil kecepatan raket saat ini.
        Vector2 velocity = rigidBody2D.velocity;

        //Jika pemain menekan tombol atas + bawah sekaligus, beri raket kecepatan 0 (diam).
        if(Input.GetKey(upButton) && Input.GetKey(downButton))
        {
            velocity.y = 0;
        }
        
        //Jika pemain menekan tombol atas, beri raket kecepatan positif ke komponen y (ke atas).
        else if (Input.GetKey(upButton))
        {
            velocity.y = speed;
        }

        //Jika pemain menekan tombol bawah, beri raket kecepatan negatif ke komponen y (ke bawah).
        else if (Input.GetKey(downButton))
        {
            velocity.y = -speed;
        }

        //Jika pemain tidak memencet tombol apapun, beri raket kecepatan 0 (diam).
        else
        {
            velocity.y = 0f;
        }

        //Masukkan kembali nilai kecepatan ke rigidBody2D.
        rigidBody2D.velocity = velocity;

        //Dapatkan posisi raket saat ini.
        Vector3 position = transform.position;

        //Jika posisi raket melebihi batas scene, bawa kembali raket ke batas scene.
        if(position.y > yBoundary)
        {
            position.y = yBoundary;
        }

        else if(position.y < -yBoundary)
        {
            position.y = -yBoundary;
        }

        //Masukkan kembali posisi ke transform.
        transform.position = position;
    }

    //Menaikkan skor sebanyak 1.
    public void IncrementScore()
    {
        score++;
    }

    //Reset skor kembali ke 0.
    public void ResetScore()
    {
        score = 0;
    }

    //Mendapatkan nilai skor.
    public int Score
    {
        get { return score; }
    }

    //Untuk mengakses informasi titik kontak dari kelas lain.
    public ContactPoint2D LastContactPoint
    {
        get { return lastContactPoint; }
    }

    //Ketika terjadi tumbukan dengan bola, rekam titik kontaknya.
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Ball"))
        {
            lastContactPoint = collision.GetContact(0);
        }
    }

    //Memanjangkan ukuran raket.
    public void IncreaseSize()
    {
        transform.localScale = new Vector3(3,4,1);
    }

    //Mengembalikan raket ke ukuran semula.
    public void ResetSize()
    {
        transform.localScale = new Vector3(3, 3, 1);
    }
}