﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    //Skrip, collider, dan rigidbody bola.
    public BallControl ball;
    CircleCollider2D ballCollider;
    Rigidbody2D ballRigidBody;

    //Bola bayangan yang akan ditampilkan pada titik tumbukan.
    public GameObject ballAtCollision;

    // Start is called before the first frame update
    void Start()
    {
        ballRigidBody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Inisiasi status pantulan lintasan yang hanya akan ditampilkan bila lintasan bertumbukan dengan objek tertentu.
        bool drawBallAtCollision = false;

        //Titik tumbukan yang digeser untuk menggambar ballAtCollision.
        Vector2 offsetHitPoint = new Vector2();

        //Tentukan titik tumbukan dengan mendeteksi pergerakan lingkaran.
        RaycastHit2D[] circleCastHit2DArray =
            Physics2D.CircleCastAll(ballRigidBody.position, ballCollider.radius, ballRigidBody.velocity.normalized);

        //Untuk setiap titik tumbukan,
        foreach(RaycastHit2D circleCastHit2D in circleCastHit2DArray)
        {
            //Jika terjadi tumbukan dan tumbukan tersebut tidak dengan bola,
            if(circleCastHit2D.collider != null && circleCastHit2D.collider.GetComponent<BallControl>() == null)
            {
                //Gambar garis lintasan dari titik tengah bola saat ini ke titik tengah bola pada saat tumbukan,
                //yaitu sebuah titik offset dari titik tumbukan berdasarkan vektor normal titik tersebut sebesar
                //jari-jari bola.

                //Tentukan titik tumbukan.
                Vector2 hitPoint = circleCastHit2D.point;
                Vector2 hitNormal = circleCastHit2D.normal;

                //Tentukan offsetHitPoint, yaitu titik tengah bola pada saat bertumbukan.
                offsetHitPoint = hitPoint + hitNormal * ballCollider.radius;

                //Gambar garis lintasan dari titik tengah bola saat ini ke titik tengah bola pada saat bertumbukan.
                DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);

                //Jika tumbukan bukan dengan sidewall, gambar pantulannya.
                if(circleCastHit2D.collider.GetComponent<SideWalls>() == null)
                {
                    //Hitung vektor datang.
                    Vector2 inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;

                    //Hitung vektor keluar.
                    Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

                    //Hitung dot product dari outVector dan hitNormal yang akan digunakan sebagai garis lintasan ketika
                    //terjadi tumbukan yang tidak tergambar.
                    float outDot = Vector2.Dot(outVector, hitNormal);
                    if(outDot > -1.0f && outDot < 1)
                    {
                        //Gambar lintasan pantulannya.
                        DottedLine.DottedLine.Instance.DrawDottedLine(offsetHitPoint, offsetHitPoint + outVector * 10.0f);

                        //Gambar bola "bayangan" pada titik tumbukan.
                        drawBallAtCollision = true;
                    }
                }

                //Karena hanya perlu menggambar lintasan untuk satu titik tumbukan, keluar loop.
                break;
            }
        }
        //Jika drawBallAtCollision bernilai true, gambar bola "bayangan" pada prediksi titik tumbukan.
        if (drawBallAtCollision)
        {
            ballAtCollision.transform.position = offsetHitPoint;
            ballAtCollision.SetActive(true);
        }
        else
        {
            //Jika false, sembunyikan bola "bayangan"
            ballAtCollision.SetActive(false);
        }
    }
}
