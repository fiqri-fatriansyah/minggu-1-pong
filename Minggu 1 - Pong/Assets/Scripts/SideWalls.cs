﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWalls : MonoBehaviour
{
    //Pemain yang akan bertambah skornya jika bola menyentuh dinding ini.
    public PlayerControl player;

    // Skrip GameManager untuk mengakses skor maksimal
    [SerializeField]
    public GameManager gameManager;

    //Audio saat bola menabrak samping.
    public AudioSource sideWallsSFX;

    // Start is called before the first frame update
    void Start()
    {
        sideWallsSFX = GetComponent<AudioSource>();
    }

    //Akan dipanggil ketika objek bola bersentuhan dengan dinding.
    void OnTriggerEnter2D(Collider2D ball)
    {
        //Jika objek yang bertumbur bernama Ball, tambahkan skor ke pemain.
        if (ball.name == "Ball")
        {
            player.IncrementScore();

            //Mainkan suara.
            sideWallsSFX.Play();

            //Jika pemain belum mencapai skor maksimal, reset ulang bola ke titik semula.
            if (player.Score < gameManager.maxScore)
            {
                ball.gameObject.SendMessage("RestartGame", null, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
