﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    //Pemain 1 dan 2.
    public PlayerControl player1;
    public PlayerControl player2;

    //Timer power up.
    public int powerUpTime;
    public int sizeUpDuration;
    private int powerUpCooldown;
    private int duration;

    //Rigid Body untuk bola.
    public BallControl ball;
    private Rigidbody2D ballRigidBody;

    //Audio saat power up didapatkan pemain.
    public AudioSource powerUpSFX;

    // Start is called before the first frame update
    void Start()
    {
        ballRigidBody = ball.GetComponent<Rigidbody2D>();
        duration = -1;
        powerUpCooldown = powerUpTime;
        Invisible();
        powerUpSFX = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (powerUpCooldown > 0)
        {
            powerUpCooldown -= 1;
        }
        else if(powerUpCooldown == 0)
        {
            transform.position = new Vector2(Random.Range(-13, 13), Random.Range(-10, 8));
            powerUpCooldown = -1;
        }
        if (duration > 0)
        {
            duration -= 1;
        }
        else if (duration == 0)
        {
            player1.ResetSize();
            player2.ResetSize();
            powerUpCooldown = powerUpTime;
            duration = -1;
        }
    }

    //Akan dipanggil ketika objek bola bersentuhan dengan power up.
    void OnTriggerEnter2D(Collider2D ball)
    {
        //Jika objek yang bertumbur bernama Ball,
        if (ball.name == "Ball")
        {
            //Pindahkan power up ke luar area permainan.
            Invisible();
            duration = sizeUpDuration;

            //Panjangkan raket pemain berdasarkan arah bola.
            if (ballRigidBody.velocity.x > 0)
            {
                player1.IncreaseSize();
            }
            else if(ballRigidBody.velocity.x < 0)
            {
                player2.IncreaseSize();
            }
        }
        //Mainkan suara.
        powerUpSFX.Play();
    }

    //Buat power up di luar area permainan.
    void Invisible()
    {
        transform.position = new Vector3(50, 50, 0);
    }

    //Reset power up ke keadaan semula.
    public void Reset()
    {
        duration = 0;
        powerUpCooldown = powerUpTime;
        Invisible();
    }
}
