﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    //Rigidbody bola.
    private Rigidbody2D rigidBody2D;

    //Besar gaya awal yang diberikan untuk mendorong bola.
    public float xInitialForce;
    public float yInitialForce;

    //Titik asal lintasan bola saat ini.
    private Vector2 trajectoryOrigin;

    //Audio saat bola bertumbukan.
    public AudioSource ballBounceSFX;

    //Object power up
    public PowerUp powerup;

    void ResetBall()
    {
        //Reset posisi bola kembali ke (0,0).
        transform.position = Vector2.zero;

        //Reset kecepatan bola menjadi 0 (diam).
        rigidBody2D.velocity = Vector2.zero;
        powerup.Reset();
    }

    void PushBall()
    {
        //Tentukan nilai komponen y dari gaya dorong antara -yInitialForce dan yInitialForce secara acak.
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);

        //Tentukan nilai acak untuk menentukan arah gaya dorong x.
        int randomDirection = Random.Range(0, 2);

        //Tentukan nilai gaya dorong x berdasarkan nilai gaya dorong y sehingga besar kecepatan bola akan selalu sama ke mana pun arahnya.
        xInitialForce = Mathf.Sqrt(2250000f - yRandomInitialForce * yRandomInitialForce);

        //Bila nilai di bawah 1, gaya dorong menjadi -x (kiri) dengan nilai acak. Bila nilai tidak di bawah 1, gaya dorong menjadi x (kanan).
        if(randomDirection < 1)
        {
            //Gunakan gaya untuk menggerakkan bola.
            rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));
        }
        else
        {
            rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce));
        }
    }

    void RestartGame()
    {
        //Kembalikan bola ke posisi semula.
        ResetBall();

        //Setelah 2 detik, beri gaya ke bola.
        Invoke("PushBall", 2);
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        ballBounceSFX = GetComponent<AudioSource>();

        //Mulai game
        RestartGame();
        trajectoryOrigin = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Ketika bola beranjak dari sebuah tumbukan, rekam titik tumbukan tersebut.
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;

        ballBounceSFX.Play();
    }

    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }
}
